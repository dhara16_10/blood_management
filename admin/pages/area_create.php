<!DOCTYPE html>
<html lang="en">

<head>
<?php 
  include("sesstion_val.php");
  include("header.php");

  $sql="select * from area";
  $record=mysqli_query($conn,$sql);

  $reco=mysqli_query($conn,"select id ,name From city");
?>
</head>

<body class="g-sidenav-show  bg-gray-200">
  <?php 
    include("sidebar.php");
  ?>
  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->
   <?php  include("dash-nav.php"); ?>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
      <div class="row">

        <div class="col-12">
          <div class="card my-4">
            <div class="card m-b-20">
              <div class="card-body">
                <div class="col">
                  <h3>Area Add</h3><hr/>
                </div>
                  <form method="post" class="form-horizontal" action="area_create.php">
                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">Name</label>
                          <div class="col-sm-6">
                              <input class="form-control" type="text" value=""  style="border:1px solid;" id="name" name="name" required>
                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">City </label>
                          <div class="col-sm-6">
                            <select name="cid" required>
                              <option> select City </option>
                              <?php
                                  while($data = mysqli_fetch_array($reco)){
                                      echo "<option value='". $data['id'] ."'>" .$data['name'] ."</option>";
                                  }
                              ?>
                            </select>
                              
                          </div>
                      </div>
                      <div class="form-group text-right" style="margin-top: 10px;">
                          <button type="submit" class="btn btn-primary" name="Submit" value="Submit">Add</button>
                      </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
     
      <?php  include("dash-foot.php"); ?>
    </div>
  <!--   Core JS Files   -->
  <?php
  include("footer.php");
  ?>
</body>
</html>
<?php
    if(isset($_POST['Submit'])){
      include_once("../../config.php");
      extract($_POST);
      $sql1="INSERT INTO `area` (`name`,`cid`) VALUES ('$name','$cid')";
      if($record= mysqli_query($conn,$sql1)){
        /*header("Location:city_view.php");exit;*/
        echo "<script>window.location.href='area_view.php';</script>";
      }else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
      }
      mysqli_close($conn);
  }
?>
  