<!DOCTYPE html>
<html lang="en">

<head>

<?php 

  include("sesstion_val.php");
  include("header.php");

  $sql="select d.id,b.name as bname,u.fname as fname,d.create_date,d.volume from donor d,blood_grp b,user u where d.bid=b.id and u.id=d.uid";
  $record=mysqli_query($conn,$sql);

?>
</head>

<body class="g-sidenav-show  bg-gray-200">
  <?php 
    include("sidebar.php");
  ?>
  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->
   <?php  include("dash-nav.php"); ?>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
              <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3 row">
                <div class="col-sm-6">
                  <h6 class="text-white text-capitalize ps-3">Donor</h6>
                </div>
                <div class="col-sm-6" style="text-align:right;x`">
                  <a  href="city_create.php" style="text-align:r">Create</a>
                </div>
              </div>
            </div>
            <div class="card-body px-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0" border="1">
                  <thead>
                    <tr>
                      <th >Id</th>
                      <th >blood </th>
                      <th >User </th>
                      <th >Create Date</th>
                      <th >Volume</th>
                      <th >Action</th>
                      <th class="text-secondary opacity-7"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      while($data = mysqli_fetch_array($record)){
                        ?>
                        <tr>
                          <td><?php echo $data['id'];?></td>
                          <td><?php echo $data['bname'];?></td>
                          <td><?php echo $data['fname'];?></td>
                          <td><?php echo $data['create_date'];?></td>
                          <td><?php echo $data['volume'];?></td>
                          <td>
                            
                            <a href="donor_delete.php?id=<?php echo $data['id'];?>" class="btn btn-primary" data-toggle="tooltip" data-original-title="Edit user">
                              Delete
                            </a>
                          </td>
                        </tr>
                    <?php 
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
     
      <?php  include("dash-foot.php"); ?>
    </div>
  <!--   Core JS Files   -->
  <?php
  include("footer.php");
  ?>
</body>

</html>