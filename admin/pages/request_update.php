<!DOCTYPE html>
<html lang="en">

<head>
<?php 

  include("sesstion_val.php");
  include("header.php");

 $id=$_GET['id'];
  
  $sql="select * from request where id=$id";
  $record=mysqli_query($conn,$sql);
  $row=mysqli_fetch_assoc($record);
  
  $reco=mysqli_query($conn,"select * From blood_grp");
  
?>
</head>

<body class="g-sidenav-show  bg-gray-200">
  <?php 
    include("sidebar.php");
  ?>
  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->
   <?php  include("dash-nav.php"); ?>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
      <div class="row">

        <div class="col-12">
          <div class="card my-4">
            <div class="card m-b-20">
              <div class="card-body">
                <div class="col">
                  <h3>Request Update</h3><hr/>
                </div>
                                    <form method="post" class="form-horizontal">
                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">Email</label>
                          <div class="col-sm-6">
                              <input class="form-control" type="text"   style="border:1px solid;" id="email"  name="email" value= "<?php echo $row['email']; ?>" required>
                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">Phone Number</label>
                          <div class="col-sm-6">
                              <input class="form-control" type="text"   style="border:1px solid;" id="phoneno" name="phoneno" value= "<?php echo $row['phoneno']; ?>" required>
                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">Blood </label>
                          <div class="col-sm-6">
                            <select name="bid" class ="form-control" style="border:1px solid;"  value= "<?php echo $row['bid']; ?>">
                              <option> select Blood Name </option>
                              <?php
                                  while($data = mysqli_fetch_array($reco)){ ?>
                                    <option value="<?php echo $data['id'];?>" <?php if($row['bid'] == $data['id'])  { echo "selected";} ?>><?php echo $data['name'];?></option>
                                 <?php  }
                              ?>
                            </select>
                              
                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">Status</label>
                          <div class="col-sm-6">
                              <select name="status" class ="form-control" style="border:1px solid;">
                                <option> Select Status </option>
                              <option value="Complete" <?php if($row['status'] == "Complete"){echo "selected";} ?>> Complete </option>
                              <option value="Pending" <?php if($row['status'] == "Pending"){echo "selected";} ?>> Pending </option>
                              <option value="Reject" <?php if($row['status'] == "Reject"){echo "selected";} ?>> Reject </option>
                            </select>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">Volume</label>
                          <div class="col-sm-6">
                              <input class="form-control" type="text" value= "<?php echo $row['volume']; ?>"  style="border:1px solid;" id="volume" name="volume" required>
                          </div>
                      </div>
                      <input class="form-control" type="hidden" value= "<?php echo $id; ?>"  style="border:1px solid;" id="id" name="id" required>
                      <div class="form-group text-right" style="margin-top: 10px;">
                          <button type="submit" class="btn btn-primary" name="Submit" value="Submit">Update</button>
                      </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
     
      <?php  include("dash-foot.php"); ?>
    </div>
  <!--   Core JS Files   -->
  <?php
  include("footer.php");
  ?>
</body>
</html>
<?php
    if(isset($_POST['Submit'])){
      include_once("../../config.php");
      extract($_POST);
      if($status == "Complete"){
        $que="select volume from blood_grp  where id=$bid";
        $rec=mysqli_query($conn,$que);
        $drow=mysqli_fetch_assoc($rec);

        $count=$drow['volume'];
        
        $count-=$volume;
        
        $up="UPDATE `blood_grp` SET `volume`='$count' WHERE id =$bid";
        $recordup= mysqli_query($conn,$up);     
      }

      $update="UPDATE `request` SET `email`='$email',`phoneno`='$phoneno',`bid`='$bid',`status`='$status',`volume`='$volume' WHERE id=$id";
      if($record= mysqli_query($conn,$update)){
        echo "<script>window.location.href='request_view.php';</script>";
      }else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
      }
     mysqli_close($conn);
    }
?>
  