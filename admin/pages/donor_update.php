<!DOCTYPE html>
<html lang="en">

<head>
<?php 
  include("sesstion_val.php");
  include("header.php");

  $id= $_GET['id'];


  $do="select * from donor where id=$id";
  $donor=mysqli_query($conn,$do);
  $do_data=mysqli_fetch_assoc($donor);  
    
  $sql="select * from blood_grp";
  $record=mysqli_query($conn,$sql);


  $sql1="select * from user";
  $record1=mysqli_query($conn,$sql1);
?>
</head>

<body class="g-sidenav-show  bg-gray-200">
  <?php 
    include("sidebar.php");
  ?>
  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->
   <?php  include("dash-nav.php"); ?>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
      <div class="row">

        <div class="col-12">
          <div class="card my-4">
            <div class="card m-b-20">
              <div class="card-body">
                <div class="col">
                  <h3>Donor Update</h3><hr/>
                </div>
                  <form method="post" class="form-horizontal" action="donor_update.php">
                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">Blood Group</label>
                          <div class="col-sm-6">
                              <select name="bid" class="form-control" style="border:1px solid;">
                                <option> select Blood Group </option>
                              <?php
                                  while($data = mysqli_fetch_array($record)){
                                      ?>
                                      <option value="<?php echo $data['id'];?>" <?php if($do_data['bid'] == $data['id']) { echo "selected";}else{echo "";} ?>><?php echo $data['name'];?></option>
                                  <?php }
                              ?>
                              </select>  
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">User</label>
                          <div class="col-sm-6">
                              <select name="uid" class="form-control" style="border:1px solid;">
                                <option value=""> Select User </option>
                              <?php
                                  while($data1 = mysqli_fetch_array($record1)){ ?>
                                      
                                      <option value="<?php echo $data1['id'];?>" <?php if($do_data['uid'] == $data1['id']) { echo "selected";} ?>><?php echo $data1['fname'];?></option>
                                  <?php }
                              ?>
                              </select>  
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">Volume</label>
                          <div class="col-sm-6">
                              <input type="number" disabled class="form-control" style="border:1px solid;" name="volume" required value="<?php echo $do_data['volume'];?>"> 
                          </div>
                      </div>
                     
                      <div class="form-group text-right" style="margin-top: 10px;">
                          <input type="hidden" name="id" value="<?php echo $id;?>"> 
                          <button type="submit" class="btn btn-primary" name="Submit" value="Submit">Update</button>
                      </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
     
      <?php  include("dash-foot.php"); ?>
    </div>
  <!--   Core JS Files   -->
  <?php
  include("footer.php");
  ?>
</body>
</html>
<?php
    if(isset($_POST['Submit'])){
      include_once("../../config.php");
      extract($_POST);
      $date = date('Y-m-d');
      
      $sql1="UPDATE `donor` SET `bid`='$bid',`uid`='$uid',`create_date`='$date' WHERE id=$id";
      if($record= mysqli_query($conn,$sql1)){
        echo "<script>window.location.href='donor_view.php';</script>";
      }else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
      }
      mysqli_close($conn);
  }
?>
  