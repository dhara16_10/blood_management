<!DOCTYPE html>
<html lang="en">
    <head>
        <?php 
          include("sesstion_val.php");
          include("header.php");
          $query= "select * from blood_grp";
          $store=mysqli_query($conn,$query);
          
          function countBloodGroup($id){
              include("../../config.php");
              $sql2="select sum(volume) as total from donor where bid = $id";
              $store1=mysqli_query($conn,$sql2);
              $row=mysqli_fetch_assoc($store1);
              return $row['total'];
          }
          $que="select count(volume) as dtotal from donor";
          $rec=mysqli_query($conn,$que);
          $drow=mysqli_fetch_assoc($rec);

          $que1="SELECT count(*) as c,status FROM `request` group by status;";
          $rec1=mysqli_query($conn,$que1);
          
        ?>

    </head>

    <body class="g-sidenav-show  bg-gray-200">
      
      <?php  include("sidebar.php"); ?>

      <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <!-- Navbar -->
        <?php  include("dash-nav.php"); ?>
        
        <!-- End Navbar -->
        <div class="container-fluid py-4">
          <div class="row">
              <?php
                while($var = mysqli_fetch_array($store)){
              ?>
              <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
                <div class="card">
                  <div class="card-header p-3 pt-2">
                    <div class="icon icon-lg icon-shape bg-gradient-dark shadow-dark text-center border-radius-xl mt-n4 position-absolute">
                      <i class="material-icons opacity-10">weekend</i>
                    </div>
                    <div class="text-end pt-1">
                      <h4 class="mb-0"><?php  echo $var['name']; ?></h4>
                      <p class="text-sm mb-0 text-capitalize">Volume : <?php echo countBloodGroup($var['id']); ?></p>
                    </div>
                  </div>
                  <hr class="dark horizontal my-0">
                  <div class="card-footer p-3">
                    <p class="mb-0"><span class="text-success text-sm font-weight-bolder">&nbsp</span></p>
                  </div>
                </div>
              </div>
            <?php } ?>
         <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
                <div class="card">
                  <div class="card-header p-3 pt-2">
                    <div class="icon icon-lg icon-shape bg-gradient-dark shadow-dark text-center border-radius-xl mt-n4 position-absolute">
                      <i class="material-icons opacity-10">weekend</i>
                    </div>
                    <div class="text-end pt-1">
                      <h4 class="mb-0">Donor</h4>
                      <p class="text-sm mb-0 text-capitalize">Total : <?php echo $drow['dtotal']; ?> </p>
                    </div>
                  </div>
                  <hr class="dark horizontal my-0">
                  <div class="card-footer p-3">
                    <p class="mb-0"><span class="text-success text-sm font-weight-bolder">&nbsp</span></p>
                  </div>
                </div>
              </div>
         </div><br>
         <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
                <div class="card">
                  <div class="card-header p-3 pt-2">
                    <div class="icon icon-lg icon-shape bg-gradient-dark shadow-dark text-center border-radius-xl mt-n4 position-absolute">
                      <i class="material-icons opacity-10">weekend</i>
                    </div>
                    <div class="text-end pt-1">
                      <h4 class="mb-0">Request</h4>
                      <?php while($drow1= mysqli_fetch_array($rec1)){ ?>
                        <p class="text-sm mb-0 text-capitalize"> <?php echo $drow1['status']; ?> : <?php echo $drow1['c']; ?> </p>
                      <?php } ?>
                    </div>
                  </div>
                  <hr class="dark horizontal my-0">
                  <div class="card-footer p-3">
                    <p class="mb-0"><span class="text-success text-sm font-weight-bolder">&nbsp</span></p>
                  </div>
                </div>
              </div>
         </div> 
        </div>

        <?php  include("dash-foot.php"); ?>
    </main>
      <!--   Core JS Files   -->
      <?php
      include("footer.php");
      ?>
    </body>
</html>