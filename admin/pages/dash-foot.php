<footer class="footer py-4  ">
	<div class="container-fluid" style="text-align:center;">
	  	<div class="row align-items-center justify-content-lg-between">
			<div class="col-lg-12 mb-lg-0 mb-4 text-center" >
		  		<div class="copyright text-center text-sm text-muted text-lg-start">
					<div class="test" style="margin: auto;width: fit-content;">
			  			© <script>
				  			document.write(new Date().getFullYear())
			  			</script>,
			  			made with <i class="fa fa-heart"></i> by
			  			<a href="https://www.creative-tim.com" class="font-weight-bold" target="_blank">Dhara & Krishna</a>
					</div>
		  		</div>
			</div>
	  </div>
	</div>
</footer>