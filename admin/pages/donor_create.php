<!DOCTYPE html>
<html lang="en">

<head>
<?php 
  include("sesstion_val.php");
  include("header.php");

  $sql="select * from blood_grp";
  $record=mysqli_query($conn,$sql);


  $sql1="select * from user";
  $record1=mysqli_query($conn,$sql1);
?>
</head>

<body class="g-sidenav-show  bg-gray-200">
  <?php 
    include("sidebar.php");
  ?>
  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->
   <?php  include("dash-nav.php"); ?>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
      <div class="row">

        <div class="col-12">
          <div class="card my-4">
            <div class="card m-b-20">
              <div class="card-body">
                <div class="col">
                  <h3>Donor Add</h3><hr/>
                </div>
                  <form method="post" class="form-horizontal" action="donor_create.php">
                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">Blood Group</label>
                          <div class="col-sm-6">
                              <select name="bid" class="form-control" style="border:1px solid;">
                                <option> select Blood Group </option>
                              <?php
                                  while($data = mysqli_fetch_array($record)){
                                      echo "<option value='". $data['id'] ."'>" .$data['name'] ."</option>";
                                  }
                              ?>
                              </select>  
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">User</label>
                          <div class="col-sm-6">
                              <select name="uid" class="form-control" style="border:1px solid;">
                                <option value=""> Select User </option>
                              <?php
                                  while($data1 = mysqli_fetch_array($record1)){
                                      echo "<option value='".$data1['id']."'>" .$data1['fname'].' '.$data1['lname']."</option>";
                                  }
                              ?>
                              </select>  
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">Volume</label>
                          <div class="col-sm-6">
                              <input type="number" class="form-control" style="border:1px solid;" name="volume" required> 
                          </div>
                      </div>
                      
                      
                      <div class="form-group text-right" style="margin-top: 10px;">
                          <button type="submit" class="btn btn-primary" name="Submit" value="Submit">Add</button>
                      </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
     
      <?php  include("dash-foot.php"); ?>
    </div>
  <!--   Core JS Files   -->
  <?php
  include("footer.php");
  ?>
</body>
</html>
<?php
    if(isset($_POST['Submit'])){
      include_once("../../config.php");
      extract($_POST);
      $date = date('Y-m-d');

      $que="select volume from blood_grp  where id=$bid";
      $rec=mysqli_query($conn,$que);
      $drow=mysqli_fetch_assoc($rec);

      $count=$drow['volume'];
      
      $count+=$volume;
      
      $update="UPDATE `blood_grp` SET `volume`='$count' WHERE id =$bid";
      $recordup= mysqli_query($conn,$update);      

      $sql1="INSERT INTO `donor`(`bid`, `uid`, `create_date`, `volume`) VALUES ('$bid','$uid','$date','$volume')";
      if($record= mysqli_query($conn,$sql1)){
        echo "<script>window.location.href='donor_view.php';</script>";
      }else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
      }
      mysqli_close($conn);
  }
?>
  