<?php 
  include("sesstion_val.php");
  include("header.php");

  $sql="select * from blood_grp";
  $record=mysqli_query($conn,$sql);

  $userid=$_SESSION['id'];

  $sql1="select * from user where id=$userid";
  $record1=mysqli_query($conn,$sql1);
  $userdata=mysqli_fetch_assoc($record1);
?>
<!DOCTYPE html>
<html lang="en">

<head>
</head>

<body class="g-sidenav-show  bg-gray-200">
  <?php 
    include("sidebar.php");
  ?>
  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->
   <?php  include("dash-nav.php"); ?>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
      <div class="row">

        <div class="col-12">
          <div class="card my-4">
            <div class="card m-b-20">
              <div class="card-body">
                <div class="col">
                  <h3>Update Profile</h3><hr/>
                </div>
                  <form method="post" class="form-horizontal" action="profile.php">
                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">First Name</label>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" name="fname" style="border:1px solid;" value="<?php echo $userdata['fname'];?>">
                          </div>
                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">Last Name</label>
                          <div class="col-sm-6">
                              <input type="text" class="form-control" name="lname" style="border:1px solid;" value="<?php echo $userdata['lname'];?>" >
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">Address</label>
                          <div class="col-sm-6">
                              <textarea name="address"  class="form-control" required placeholder="Enter youe address" style="border:1px solid;"><?php echo $userdata['address'];?></textarea>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">Gender</label>><br>
                      </div>
                      <div>
                        <label class="form-label">Male</label>
                          <input type="radio" name="gender" value="male" <?php if($userdata['gender'] == "male"){echo "checked";} ?> >
                          <label class="form-label">Female</label>
                          <input type="radio" name="gender" value="female"  <?php if($userdata['gender'] == "female"){echo "checked";} ?>>
                          <div class="form-group text-right" style="margin-top: 10px;">
                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">Phone Number</label>
                          <div class="col-sm-6">
                              <input type="number" class="form-control" name="phoneno" style="border:1px solid;" value="<?php echo $userdata['phoneno'];?>"> 
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">Email</label>
                          <div class="col-sm-6">
                              <input type="email" class="form-control" name="email" style="border:1px solid;" value="<?php echo $userdata['email'];?>"> 
                          </div>
                      </div>
                      <div class="form-group row">
                          <div class="col-sm-6">
                              <input type="hidden" class="form-control" name="id" style="border:1px solid;" value="<?php echo  $userdata['id'];?>"> 
                          </div>
                      </div>

                          <button type="submit" class="btn btn-primary" name="Submit" value="Submit" style="    margin-top: 20px">Update</button>
                      </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
     
      <?php  include("dash-foot.php"); ?>
    </div>
  <!--   Core JS Files   -->
  <?php
  include("footer.php");
  ?>
</body>
</html>
<?php
    if(isset($_POST['Submit'])){
      include_once("../../config.php");
      
      extract($_POST);
      $date = date('Y-m-d');
      $sql1="UPDATE `user` SET `fname`='$fname',`lname`='$lname',`email`='$email',`address`='$address',`gender`='$gender',`phoneno`='$phoneno' WHERE id=$id";
      if($record= mysqli_query($conn,$sql1)){
        echo "<script>window.location.href='profile.php';</script>";
      }else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
      }
      mysqli_close($conn);
  }
?>
  