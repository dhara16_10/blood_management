<html lang="en">

<head>
  <?php
    include("header.php");
    $records = mysqli_query($conn,"select id,name From area");
  ?>
</head>

<body class="">
  <div class="container position-sticky z-index-sticky top-0">
    <div class="row">
      <div class="col-12">
        <!-- Navbar -->
        
        <!-- End Navbar -->
      </div>
    </div>
  </div>
  <main class="main-content  mt-0">
    <section>
      <div class="page-header min-vh-100">
        <div class="container">
          <div class="row">
            <div class="col-6 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 start-0 text-center justify-content-center flex-column">
              <div class="position-relative bg-gradient-primary h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center" style="background-image: url('../assets/img/illustrations/signimg.jfif'); background-size: cover;">
              </div>
            </div>
            <div class="col-xl-4 col-lg-5 col-md-7 d-flex flex-column ms-auto me-auto ms-lg-auto me-lg-5">
              <div class="card card-plain">
                <div class="card-header">
                  <h4 class="font-weight-bolder">Sign Up</h4>
                  <p class="mb-0">Enter your email and password to register</p>
                </div>
                <div class="card-body">
                  <form role="form" action="register_action.php" method="post">
                    <div class="input-group input-group-outline mb-3">
                      <label class="form-label">First Name</label>
                      <input type="text" class="form-control" name="fname" required >
                    </div>
                    <div class="input-group input-group-outline mb-3">
                      <label class="form-label">Last Name</label>
                      <input type="text" class="form-control" name="lname" required>
                    </div>
                    <div class="input-group input-group-outline mb-3">
                      <textarea name="address"  class="form-control" required ></textarea>
                    </div>
                    <div class="input-group input-group-outline mb-3">
                      <label class="form-label">Gender</label><br>
                    </div>
                    <div>
                    <label class="form-label">Male</label>
                      <input type="radio" name="a" value="male" required>
                      <label class="form-label">Female</label>
                      <input type="radio" name="a" value="female" required>
                    </div>  
                    <div class="input-group input-group-outline mb-3">
                      <label class="form-label">Phone Number</label>
                      <input type="number" name="pno" class="form-control" required>
                    </div>
                    <div class="input-group input-group-outline mb-3">
                      <label class="form-label">Email</label>
                      <input type="email" name="email" class="form-control" required>
                    </div>
                    <div class="input-group input-group-outline mb-3">
                      <label class="form-label">Password</label>
                      <input type="password" name="password" class="form-control" required>
                    </div>
                    <div class="input-group input-group-outline mb-3">
                      
                      <select name="area" class="form-control" required>
                        <option value="" >Select Area</option>
                        <?php
                          while($data = mysqli_fetch_array($records)){
                              echo "<option value='". $data['id'] ."'>" .$data['name'] ."</option>";
                          }
                        ?>
                      </select>
                      </div>
                      <div class="card-footer text-center pt-0 px-lg-2 px-1">
                        <input type="submit" name="submit" value="Submit" class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0">
                      </div>
                  </form>
                </div>
                <div class="card-footer text-center pt-0 px-lg-2 px-1">
                  <p class="mb-2 text-sm mx-auto">
                    Already have an account?
                    <a href="../pages/sign-in.php" class="text-primary text-gradient font-weight-bold">Sign in</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  
  <!--   Core JS Files   -->
  <?php 
    include("footer.php");
  ?>
</body>

</html>

