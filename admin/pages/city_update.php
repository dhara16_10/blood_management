<!DOCTYPE html>
<html lang="en">

<head>
<?php 

  include("sesstion_val.php");
  include("header.php");

  $id=$_GET['id'];

  $sql="select name from city where id=$id";
  $record=mysqli_query($conn,$sql);
  $row=mysqli_fetch_assoc($record);

?>
</head>

<body class="g-sidenav-show  bg-gray-200">
  <?php 
    include("sidebar.php");
  ?>
  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->
   <?php  include("dash-nav.php"); ?>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
      <div class="row">

        <div class="col-12">
          <div class="card my-4">
            <div class="card m-b-20">
              <div class="card-body">
                <div class="col">
                  <h3>City Update</h3><hr/>
                </div>
                  <form method="post" class="form-horizontal" action="city_update.php">
                      <div class="form-group row">
                          <label for="name" class="col-sm-10 col-form-label">Name</label>
                          <div class="col-sm-6">
                              <input class="form-control" type="text" style="border:1px solid;" id="name" name="name" value="<?php echo $row['name']; ?>" required>
                          </div>
                          <div class="col-sm-6">
                              <input class="form-control" type="hidden" style="border:1px solid;"  name="id" value="<?php echo $id; ?>" required>
                          </div>
                      </div>
                      <div class="form-group text-right" style="margin-top: 10px;">
                          <button type="submit" class="btn btn-primary" name="Submit" value="Submit">Update</button>
                      </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      
     
      <?php  include("dash-foot.php"); ?>
    </div>
  <!--   Core JS Files   -->
  <?php
  include("footer.php");
  ?>
</body>
</html>
<?php
    if(isset($_POST['Submit'])){
       
      include_once("../../config.php");
      extract($_POST);
      $sql1="UPDATE `city` SET `name`='$name' WHERE id=$id;";
      if($record= mysqli_query($conn,$sql1)){
        /*header("Location:city_view.php");exit;*/
        echo "<script>window.location.href='city_view.php';</script>";
      }else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
      }
      mysqli_close($conn);
  }
?>
  